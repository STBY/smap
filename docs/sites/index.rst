.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _SMAPhaploindex:

SMAP haplotype-sites
====================

| This is the manual for the SMAP haplotype-sites component of the SMAP-package. 
| This can be the starting point for preprocessed HiPlex data, whereas for GBS data, **SMAP delineate** should be performed beforehand.
| The scheme below depicts the two major distinctions, concerning library preparation method and sample type, implemented in this program.

.. image:: ../images/sites/haplotype_step_scheme.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   sites_scope_quickstart
   LibraryPrep/index
   SampleType/index
   sites_rec_HIW
   sites_code






