.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

    <style> .navy {color:navy} </style>
	
.. role:: navy

.. raw:: html

    <style> .white {color:white} </style>

.. role:: white

.. _SMAPhaploHIWindex:

Haplotyping for different library preparation methods
=====================================================

By design, GBS and HiPlex library preparation methods yield different types of read data and should be processed in a slightly different way. 
Therefore, this manual for SMAP haplotype-sites discusses the unique features separately for GBS and HiPlex libraries.
The principal differences between GBS and HiPlex read mapping are the absence (HiPlex) or presence (GBS) of consistent Stack Mapping Anchor Points. 

:navy:`Delineation of loci for GBS`

GBS reads are derived from genomic fragments flanking restriction sites, which are ligated to adapters for sequencing. `GBS read preprocessing <https://gbprocess.readthedocs.io/en/latest/index.html>`_ efficiently and effectively removes adapter sequences and restriction site remnants. So, for a given locus, the reads begin at the same sequence and have a consistent and well-defined length. 

.. image:: ../../images/sites/SMAP_haplotype_step1_GBS.png

For GBS, the BED file defining locus positions is created using **SMAP delineate**, which identifies the actual *a priori* unknown positions of read mapping, and *de novo* discovers read mapping polymorphisms, including internal SMAPs. This approach captures the partial coverage of reads across the length of the locus as additional, genetically informative, molecular marker information, and combines that with SNPs into haplotypes. The mandatory :ref:`option <SMAPhaploquickstartcommands>` ``-partial include`` must be used to capture GBS haplotype information.  

:navy:`Delineation of loci for HiPlex`

By comparison, preprocessing of HiPlex amplicon sequencing reads is more difficult, as technical issues hamper accurate positional and pattern trimming. 
First, in highly multiplex amplicon sequencing, each amplicon contains a unique pair of primers. Trimming these primers off by pattern matching requires searching for all possible combinations of primers in each read, which is computationally prohibitive. 
Second, as each primer typically has a slightly different length (range 18-27 bp), trimming a fixed length off the 5'-end and 3'-end of each amplicon does not yield genomic fragments with a sequence exactly internal to the primers (as in GBS, genomic fragments are created flanking the restriction site remnants). 
Third, as highly multiplex amplicon sequencing reads do not always start at the first (5' end) nucleotide of the primer, positional trimming of reads by any fixed length would create a similar distribution of variable sequence start points, and thus artefactual Stack Mapping Anchor Points (SMAPs).

.. image:: ../../images/sites/SMAP_haplotype_step1_AS.png

For HiPlex, a simpler BED file defining only locus outer positions is created by listing the two reference sequence positions (primer-based anchor points) immediately interior to the primer binding sites for each amplicon (a locus start position flanking the forward primer, and a locus end position flanking the reverse primer). By design, all primer binding sites and expected amplicons are *a priori* known, and there is no need to search for alternative loci with read mapping. Read mapping polymorphisms are excluded by evaluating only reads that span the entire locus (and thus are expected to contain both primers, just outside the region used for haplotyping). The mandatory :ref:`option <SMAPhaploquickstartcommands>` ``-partial exclude`` must be used to capture HiPlex haplotype information.

:navy:`Haplotype-sites considers different locus structures for HiPlex and GBS`

To account for the differences in expected read mapping profiles between GBS and HiPlex data, while still being able to perform read-backed haplotyping with a common algorithm, two different approaches are introduced to define the outer positions of loci. 

For GBS, the outer positions of loci are determined by **SMAP delineate** and this analysis simultaneously identifies internal Stack Mapping Anchor Points (SMAPs) as polymorphisms in read mapping which can be used for additional markers sites for haplotyping. See detailed explanation in :ref:`SMAP delineate <SMAPdeldef>`.  

For HiPlex data, the start and end positions that delineate a given locus for haplotyping are simply defined by the nucleotide positions internally flanking the primer binding sites. Instead of trying to trim primer sequences off reads, these are left on the reads to support extension of the read alignment towards the respective ends of the locus, because primer sequences are expected to have a perfect match with the reference sequence on which they were initially designed. 
As a consequence, HiPlex reads are expected to cover the entire sequence region spanning the genome sequence between the primer binding sites. The BED file delineating the loci, therefore, only contains the positions immediately internal to the primer binding sites, and reads with internal mapping polymorphisms are not considered for haplotyping. 


.. image:: ../../images/sites/haplotype_step_scheme_12.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   sites_HiPlex_HIW
   sites_GBS_HIW

