.. raw:: html

    <style> .navy {color:navy} </style>
	
.. role:: navy

.. raw:: html

    <style> .white {color:white} </style>

.. role:: white

###########################
Scope & Quick Start
###########################

Scope
-----

:navy:`SMAP haplotype-sites: using polymorphic sites (SNPs and/or SMAPs) for read-backed haplotyping`

| **SMAP haplotype-sites** reconstructs multi-allelic haplotypes based on a predefined set of polymorphisms at Single Nucleotide Polymorphisms (SNPs) and/or Stack Mapping Anchor Points (:ref:`SMAPs <SMAPdeldef>`) through read-backed haplotyping.
| **SMAP haplotype-sites** should only be used for \`stacked´ \ read data such as Genotyping-By-Sequencing (GBS) or highly multiplex amplicon sequencing (HiPlex). It is *not* fit for random fragmented (e.g. Shotgun Sequencing) read data.

.. image:: ../images/sites/SMAP_sites_introduction_scheme.png

:navy:`SMAP haplotype-sites only requires this input:`
	
	1. a single BED file to define the start and end points of loci (loci created by :ref:`SMAP delineate <SMAPdelHIW>` for GBS, or amplicon regions for HiPlex).
	2. a single VCF file containing bi-allelic SNPs obtained with third-party SNP calling software.
	3. a set of indexed BAM files for all samples that need to be compared.

| **SMAP haplotype-sites** performs read-backed haplotyping, per sample, per locus, per read, using positional information of read alignments and creates multi-allelic haplotypes from a short string of polymorphic *sites* (ShortHaps).
| **SMAP haplotype-sites** takes a conservative approach, without any form of imputation or phase extension, and strictly considers SNPs and/or SMAPs within a read for read-backed haplotyping.
| **SMAP haplotype-sites** filters out genotype calls of loci with low read counts, and low frequency haplotypes, to control for noise in the data.
| **SMAP haplotype-sites** creates a multi-allelic genotype call matrix listing haplotype calls, per sample, per locus, across the sample set.
| **SMAP haplotype-sites** always returns quantitative haplotype frequencies, useful for Pool-Seq data.
| **SMAP haplotype-sites** can also create discrete haplotype calls (expressed as either dominant or dosage calls) for individual samples.
| **SMAP haplotype-sites** plots the haplotype frequency distribution per sample.
| **SMAP haplotype-sites** plots a histogram of the number of haplotypes per locus across the sample set to show the haplotype diversity.

:navy:`Loci with sets of polymorphic sites`

| In the haplotype-sites workflow, the user first selects loci known to be covered by reads across the sample set. For HiPlex data, pairs of primers define locus positions. SNPs identified by third-party software that are located within these loci are combined into haplotypes, all other SNPs and all other non-polymorphic positions are excluded. For GBS data, read mapping polymorphisms (SMAPs, see :ref:`SMAP delineate <SMAPdelsepvmerg>`) define locus positions and may be combined with SNPs as molecular markers for haplotyping. (See for third-party SNP calling software: `SAMtools <http://www.htslib.org/>`_, `BEDtools <https://bedtools.readthedocs.io/en/latest/index.html>`_, `Freebayes <https://github.com/ekg/freebayes>`_, or `GATK <https://gatk.broadinstitute.org/hc/en-us>`_ for individuals, or `SNAPE-pooled <https://github.com/EmanueleRaineri/snape-pooled>`_ for Pool-Seq data. See also `Veeckman et al, 2019 <https://academic.oup.com/dnaresearch/article/26/1/1/5133005>`_ for a comparison of methods).

----
 
.. _SMAPhaploquickstart:
 
Quick Start
-----------

.. tabs::

   .. tab:: overview
	  
	  | The scheme below shows how **SMAP haplotype-sites** is integrated with `preprocessing <https://gbprocess.readthedocs.io/en/latest/index.html>`_, :ref:`SMAP delineate <SMAPdelindex>` and SNP calling (white rounded boxes).
	  | Functions of **SMAP haplotype-sites** are shown in grey ovals. Read-reference nucleotide pairs are retrieved by `pysam <https://pysam.readthedocs.io/en/latest/api.html>`_ 's ``get_aligned_pairs`` function, in which lower case nucleotides signify \"different from the reference"\;.
	  
	  .. image:: ../images/sites/SMAP_delineate_haplotype_filter.png

   .. tab:: required input

	  .. tabs::

		 .. tab:: BED
		 
			Depending on the type of data (GBS or HiPlex), a specific BED file must be created to define the start and end positions of loci.
			
			.. tabs::

			   .. tab:: GBS
			   
				  .. image:: ../images/sites/BED_GBS.png
				  
				  |
				  | For GBS data, the user needs to run :ref:`SMAP delineate <SMAPdelHIW>` to create a BED file listing the loci with SMAPs (header is illustrative).

				  =============== ===== ===== =============================== =================== ======= ======================= ============== ======== =============
				  Reference       Start End   MergedCluster_name              Mean_read_depth     Strand  SMAPs                   Completeness   nr_SMAPs Name
				  =============== ===== ===== =============================== =================== ======= ======================= ============== ======== =============
				  scaffold_10030  15617 15711 scaffold_10030_15617_15711_+    1899                \+ \    15617,15621,15702,15710 13             4        2n_ind_GBS_SE
				  scaffold_10030  15712 15798 \scaffold_10030_15712_15798_- \ 1930                \- \    15712,15792,15797       9              3        2n_ind_GBS_SE
				  =============== ===== ===== =============================== =================== ======= ======================= ============== ======== =============
				  
				  BED file entry listing all relevant features of two neighboring loci. On the + strand of the reference sequence, the start (15617) and end (15711) positions of the locus, together with the mean locus read depth (1899), the strand (+), the internal SMAP positions (15621, 15702), the number of samples with data at that locus (completeness, 13), the number of SMAPs (4), and a custom label that denotes the dataset (2n_ind_GBS_SE). The second entry lists the locus and SMAP positions on the - strand.

			   .. tab:: HiPlex		

				  .. image:: ../images/sites/BED_HiPlex.png

				  | 
				  | For HiPlex data, the user needs to create a custom BED file listing the loci based on the primer binding sites. We recommend to keep primer sequences in HiPlex reads for mapping, but to define the region between the primers in the BED file used for **SMAP haplotype-sites**. This region is defined by the first nucleotide downstream of the forward primer binding site to the last nucleotide upstream of the reverse primer binding site.
				  
				  =============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
				  Reference       Start  End    HiPlex_locus_name            Mean_read_depth      Strand  SMAPs             Completeness   nr_SMAPs Name
				  =============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
				  scaffold_312    56971  57046  scaffold_312_56971_57046     .                    \+ \    56971,57045       .              2        HiPlex_Set1  
				  scaffold_78     209790 209868 scaffold_78_209790_209868    .                    \+ \    209790,209867     .              2        HiPlex_Set1  
				  scaffold_157    107250 107307 scaffold_157_107250_107307   .                    \+ \    107250,107306     .              2        HiPlex_Set1  
				  =============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
			
				  The primer binding site coordinates need to be transformed as follows:
			
				  ================= =====================================================
				  BED                     INPUT
				  ================= =====================================================
				  Reference         reference sequence ID
				  Start             F-primer end position + 1
				  End               R-primer start position - 1
				  HiPlex_locus_name reference_start_stop
				  Mean_Read_Depth   .
				  Strand            \+ \
				  SMAPs             F-primer end position + 1, R-primer start position - 2
				  Completeness      .
				  nr_SMAPs          2
				  Name              HiPlex_Set1
				  ================= =====================================================
		 
		 .. tab:: VCF
		 
			==================== ===== == === === ======== ====== ==== ======
			##fileformat=VCFv4.2
			-----------------------------------------------------------------
			#CHROM               POS   ID REF ALT QUAL     FILTER INFO FORMAT
			==================== ===== == === === ======== ====== ==== ======
			scaffold_10030       15623 .  G   T   68888.7  .      .    GT
			scaffold_10030       15650 .  C   T   1097.13  .      .    GT
			scaffold_10030       15655 .  A   T   1097.13  .      .    GT
			scaffold_10030       15682 .  C   G   1097.13  .      .    GT
			scaffold_10030       15689 .  T   C   1097.13  .      .    GT
			scaffold_10030       15700 .  A   C   1097.13  .      .    GT
			scaffold_10030       15704 .  G   T   1097.13  .      .    GT
			scaffold_10030       15705 .  A   C   1097.13  .      .    GT
			scaffold_10030       15733 .  C   T   45538.80 .      .    GT
			scaffold_10030       15753 .  G   C   44581.50 .      .    GT
			scaffold_10030       15769 .  C   A   64858.50 .      .    GT
			scaffold_10030       15787 .  A   C   67454.00 .      .    GT
			scaffold_10030       15796 .  A   C   45281.60 .      .    GT
			==================== ===== == === === ======== ====== ==== ======
			
			VCF file listing the 13 SNPs identified at these two loci using third-party software (see also `Veeckman et al, 2018 <https://academic.oup.com/dnaresearch/article/26/1/1/5133005>`_). In order to comply with bedtools, which generates the locus \- \ SNP overlap, a 9-column VCF format with VCFv4.2-style header is required. However, only the first 2 columns contain essential information for **SMAP haplotype-sites**, the other columns may contain data, or can be filled with \"."\.

		 .. tab:: BAM
		 		 
			.. image:: ../images/sites/scaffold_10030_ref0030940_0070_edit.png
			
			| BAM file containing the alignments of single-end GBS read data of an individual genotype, illustrating the presence of various haplotypes. The GBS fragment is flanked on both sides by a *Pst* I restriction site (grey box) and contains two independent loci. The first locus contains single-end reads mapped on the forward (+) strand. 
			| The second locus contains reads mapped on the reverse (-) strand. Haplotypes are defined by combinations of neighboring SMAPs (light blue arrows) and SNPs (purple arrows). A SMAP at position 15622 is created by an InDel close to the \5' \ of the GBS-fragment combined with a misalignment (see :ref:`SMAP delineate <SMAPdelsepvmerg>` for details), while a SMAP at position 15792 is created by consistent soft clipping in a particular haplotype. Various sequencing read errors are present at positions other than the identified SNP positions, but are ignored as they are not listed in the VCF file. One of the SNPs (15793) is located in the soft clipped region.

   .. tab:: procedure
	  
	  | **SMAP haplotype-sites** reconstructs haplotypes based on SMAP positions and SNPs through read-backed haplotyping on a given set of BAM files.
	  | **SMAP haplotype-sites** first creates sets of polymorphic positions per locus on the reference genome by intersecting locus regions (obtained with :ref:`SMAP delineate <SMAPdelHIW>`) with a VCF file containing selected SNPs (obtained from any third-party SNP calling algorithm applied to the same set of BAM files). 
	  | In each BAM file, **SMAP haplotype-sites** then evaluates each read-reference alignment for the nucleotide aligned at the SMAP/SNP positions and scores as follows:

	  ========= ===========================================================================
	  CALL TYPE CLASSES
	  ========= ===========================================================================
	  .         absence of read mapping
	  0         presence of the reference nucleotide
	  1         presence of an alternative nucleotide (any nucleotide different from the reference)
	  \- \      presence of a gap in the alignment
	  ========= ===========================================================================
	
	  These calls are concatenated into a haplotype string of \'.01-'\s. For each discovered haplotype in the data, the total number of corresponding reads is counted per sample. Next, the haplotype counts of all samples are integrated into one master table, and expressed as relative haplotype frequency per locus per sample. Haplotypes with low frequency across all samples are removed to control for noise. The final table with haplotype frequencies per locus per sample is the end point for analysis of Pool-Seq data. Using the :ref:`option <SMAPhaploquickstartcommands>` ``--discrete_calls``, **SMAP haplotype-sites** transforms the haplotype frequency table into discrete haplotype calls for individuals.

	  Three modes may be chosen for discrete haplotype calling in individuals:
	  
	  ============================= =============
	  CALL TYPE                     CLASSES
	  ============================= =============
	  dosage calls in diploids      0, 1, 2
	  dosage calls in tetraploids   0, 1, 2, 3, 4
	  dominant calls                0, 1
	  ============================= =============

	  In the following sections, identification and quantification of haplotypes is illustrated on single-end GBS read data of a set of 8 diploid individuals at two partially overlapping loci. The content of the three example input files (BED, VCF, BAM) at this locus will be used to demonstrate the subsequent steps of **SMAP haplotype-sites**.
	  

----
	  
Output
------

**Tabular output**

.. tabs::

   .. tab:: General output

      By default, **SMAP haplotype-sites** will return two .tsv files.  
 
      :navy:`haplotype counts`
      
      **Read_counts_cx_fx_mx.tsv** (with x the value per option used in the analysis) contains the read counts (``-c``) and haplotype frequency (``-f``) filtered and/or masked (``-m``) read counts per haplotype per locus as defined in the BED file from **SMAP delineate**.  
      This is the file structure:
      
		============ ========== ======= ======= ========
		Locus        Haplotypes Sample1 Sample2 Sample..
		============ ========== ======= ======= ========
		Chr1:100-200 00010      0       13      34      
		Chr1:100-200 01000      19      90      28      
		Chr1:100-200 00110      60      0       23      
		Chr1:450-600 0010       70      63      87      
		Chr1:450-600 0110       108     22      134     
		============ ========== ======= ======= ========

      :navy:`relative haplotype frequency`
      
      **Haplotype_frequencies_cx_fx_mx.tsv** contains the relative frequency per haplotype per locus in sample (based on the corresponding count table: Read_counts_cx_fx_mx.tsv). The transformation to relative frequency per locus-sample combination inherently normalizes for differences in total number of mapped reads across samples, and differences in amplification efficiency across loci.  
      This is the file structure:
      
		============ ========== ======= ======= ========
		Locus        Haplotypes Sample1 Sample2 Sample..
		============ ========== ======= ======= ========
		Chr1:100-200 00010      0       0.13    0.40    
		Chr1:100-200 01000      0.24    0.87    0.33    
		Chr1:100-200 00110      0.76    0       0.27    
		Chr1:450-600 0010       0.39    0.74    0.39    
		Chr1:450-600 0110       0.61    0.26    0.61    
		============ ========== ======= ======= ========
		
   .. tab:: Additional output for individuals
   
      For individuals, if the option ``--discrete_calls`` is used, the program will return three additional .tsv files. Their content and order of creation is shown in :ref:`this scheme <SMAPhaplostep4>`.  
      
	  | :navy:`haplotype total discrete calls`
      
	  | The first file is called **haplotypes_cx_fx_mx_discrete_calls._total.tsv** and this file contains the total dosage calls, obtained after transforming haplotype frequencies into discrete calls, using the defined ``--frequency_interval_bounds``. The total sum of discrete dosage calls is expected to be 2 in diploids and 4 in tetraploids.

		============ ======= ======= ========
		Locus        Sample1 Sample2 Sample..
		============ ======= ======= ========
		Chr1:100-200 2       2       3       
		Chr1:450-600 2       2       2       
		============ ======= ======= ========
		
	  | :navy:`haplotype discrete calls`
	  
	  | The second file is **haplotypes_cx_fx_mx-discrete_calls_filtered.tsv**, which lists the discrete calls per locus per sample after ``--dosage_filter`` has removed loci per sample with an unexpected number of haplotype calls (as listed in haplotypes_cx_fx_mx_discrete_calls_total.tsv). The expected number of calls is set with option ``-z`` [use 2 for diploids, 4 for tetraploids].

		============ ========== ======= ======= ========
		Locus        Haplotypes Sample1 Sample2 Sample..
		============ ========== ======= ======= ========
		Chr1:100-200 00010         0       1       NA   
		Chr1:100-200 01000         1       1       NA   
		Chr1:100-200 00110         1       0       NA   
		Chr1:450-600 0010          1       1       1    
		Chr1:450-600 0110          1       1       1    
		============ ========== ======= ======= ========
		  
	  | :navy:`population haplotype frequencies`

	  | The third file, **haplotypes_cx_fx_mx_Pop_HF.tsv**, lists the population haplotype frequencies (over all individual samples) based on the total number of discrete haplotype calls relative to the total number of calls per locus.

		============ ========== ====== =====
		Locus        Haplotypes Pop_HF count
		============ ========== ====== =====
		Chr1:100-200 00010      25.0   4    
		Chr1:100-200 01000      50.0   4    
		Chr1:100-200 00110      25.0   4    
		Chr1:450-600 0010       50.0   6    
		Chr1:450-600 0110       50.0   6    
		============ ========== ====== =====

	  | For individuals, if the option ``--locus_correctness`` is used in combination with ``--discrete_calls`` and ``--frequency_interval_bounds``, the programm will create a new .bed file **haplotypes_cx_fx_mx_correctnessx_loci.bed** (loci filtered from the input .bed file) containing only the loci that were correctly dosage called (-z) in at least the defined percentage of samples. :ref:`See above <SMAPhaplostep4>`.

	  | :navy:`Loci with correct calls across the sample set`

		=============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
		Reference       Start  End    HiPlex_locus_name            Mean_read_depth      Strand  SMAPs             Completeness   nr_SMAPs Name
		=============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
		Chr1            100    200    Chr1_100-200                 .                    \+ \    100,199           .              2        HiPlex_Set1  
		Chr1            450    600    Chr1_450-600                 .                    \+ \    450,599           .              2        HiPlex_Set1  
		=============== ====== ====== ============================ ==================== ======= ================= ============== ======== =============
		
**Graphical output**

:navy:`haplotype diversity`

.. tabs::

   .. tab:: haplotype diversity across sampleset
	
	 By default, **SMAP haplotype-sites** will generate graphical output summarizing haplotype diversity. haplotype_diversity_across_sampleset.png shows a histogram of the number of distinct haplotypes per locus *across* all samples.  
     
   .. tab:: example graph
	
	  .. image:: ../images/sites/haplotype_counts.cigar.barplot.png


:navy:`haplotype frequency distribution per sample`

.. tabs::

   .. tab:: haplotype frequency distribution per sample
	 
     Graphical output of the haplotype frequency distribution for each individual sample can be switched **on** using the option ``--plot_all``. sample_haplotype_frequency_distribution.png shows the haplotype frequency distribution across all loci detected per sample. It is the graphical representation of each sample-specific column in **haplotypes_cx_fx_mx.tsv**. Using the option ``--discrete_calls``, this plot will also show the defined discrete calling boundaries.

   .. tab:: example graph
	
	  .. image:: ../images/sites/2n_ind_GBS_SE_001.bam.haplotype.frequency.histogram.png

:navy:`quality of genotype calls per locus and per sample (only for individuals)`

.. tabs::

   .. tab:: QC of loci and samples using discrete dosage calls  
	
	 After discrete genotype calling with option ``--discrete_calls``, **SMAP haplotype-sites** will evaluate the observed sum of discrete dosage calls per locus per sample versus the expected value per locus (set with option ``-z``, recommended use: 2 for diploid, 4 for tetraploid). 
     
     The quality of genotype calls per *sample* is calculated in two ways: the fraction of loci with calls in that sample versus the total number of loci across all samples (sample_call_completeness); the fraction of loci with expected sum of discrete dosage calls (``-z``) versus the total number of observed loci in that sample (sample_call_correctness.tsv). These scores are calculated separately per *sample*, and **SMAP haplotype-sites** plots the distribution of those scores across the sample set (sample_call_completeness.png; sample_call_correctness.png).  
      
     Similarly, the quality of genotype calls per *locus* is calculated in two ways: the fraction of samples with calls for that locus versus the total number of samples (locus_call_completeness); the fraction of samples with expected sum of discrete dosage calls (``-z``) versus the total number of observed samples for that locus (locus_call_correctness.tsv). These scores are calculated separately per *locus*, and **SMAP haplotype-sites** plots the distribution of those scores across the locus set (locus_call_completeness.png; locus_call_correctness.png).  
      
     Both graphs and the corresponding tables (one for samples and one for loci) can be evaluated to identify poorly performing samples and/or loci. We recommend to eliminate these from further analysis by removing BAM files from the run directory and/or loci from the SMAP delineate BED file with SMAPs, and iterate through rounds of data analysis combined with sample and locus quality control.

   .. tab:: completeness and correctness across the sample set
	
	  .. image:: ../images/sites/sample_call_completeness_correctness_40canephora.png
	  
	  The sample call completeness plot shows the percentage of loci that have data across the samples after all filters. In read depth-saturated, low diversity datasets, the majority of samples should have high locus completeness and there should not be much variation in completeness between samples. In a high diversity or read depth-unsaturated sample set, locus completeness per sample will be lower and more spread out.
	  
	  The sample call correctness plot displays the percentage of correctly dosage called (``-z``) loci across the sampleset. Loci are only masked in samples with a dosage value different from ``-z`` but remain in the data set for all other samples with the expected dosage value.
	  
   .. tab:: completeness and correctness across the locus set
	
	  .. image:: ../images/sites/locus_call_completeness_correctness_40canephora.png

	  The locus call completeness plot displays the percentage of samples that have data (after every filter) on a locus for every locus. In read depth-saturated, low diversity sample sets, the majority of samples should have many high completeness loci and few low completeness loci. In a high diversity or read depth-unsaturated sample set, many loci will have a low completeness.
	  
	  The locus call correctness plot shows the percentage of samples that were correctly dosage called (``-z``) across the locus set. Loci with low correctness values indicate potential genotype calling artefacts and should be removed from the data set.

----

.. _SMAPhaploquickstartcommands:

  
Summary of Commands
-------------------

::

	smap haplotype-sites /path/to/BAM/ /path/to/BED/ /path/to/VCF/ -read_type separate --no_indels -c 10 -f 5 -p 8 --plot_type png -partial include --min_distinct_haplotypes 2 -o haplotypes_SampleSet1

Command examples and options of **SMAP haplotype-sites** for a range of specific sample types are given under :ref:`haplotype frequency profiles <SMAPhaplofreq>`.  
Options may be given in any order.

.. tabs::

   .. tab:: mandatory options
	 	  
	  | The option ``-read_type`` must always be used to specify if reads are mapped separately of if paired-end reads are merged before read mapping, by e.g; `PEAR <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3933873/>`_.
		
		| use ``-read_type separate`` for single-end or paired-end reads mapped separately.
		| *or*
		| use ``-read_type merged`` for reads that were merged before mapping.
	  
	  | The option ``-partial`` must always be used to specify if reads are expected to display read mapping polymorphisms along the locus (GBS), or if reads are expected to be aligned at both outer positions of the locus (HiPlex, full locus coverage). 
		
		| use ``-partial include`` for :ref:`GBS <SMAPhaploGBSpartial>` because you must **include** reads with mapping position polymorphisms in the haplotype table.
		| *or*
		| use ``-partial exclude`` for :ref:`HiPlex <SMAPhaploASpartial>` because reads amplified with both primers are expected to cover the entire region between the primers. This is scored by being "present" in the read-reference aligned nucleotide pair on the two SMAP positions (just downstream of the forward primer, and just upstream of the reverse primer). Reads with partial alignments are considered amplification, sequencing, or read trimming artefacts, and are excluded from evaluation in the haplotype tables.

   .. tab:: general options

	  | ``alignments_dir`` :white:`#############` *(str)* :white:`###` Path to the directory containing BAM and BAI files. All BAM files should be in the same directory. Positional mandatory argument, should be the **first** argument after ``smap haplotype-sites`` [no default].  
	  | ``bed`` :white:`#####################` *(str)* :white:`###` Path to the BED file containing sites for which haplotypes will be reconstructed. For GBS experiments, the BED file should be generated using :ref:`SMAP delineate <SMAPdelHIW>`. For HiPlex data, a BED6 file can be provided, with the 4th and 5th column being blank and the chromosome name, locus start position site, locus end position site and strand information populating the first, second, third and sixth column respectively. Positional mandatory argument, should be the **second** argument after ``smap haplotype-sites``.
	  | ``vcf`` :white:`#####################` *(str)* :white:`###` Path to the VCF file (in VCFv4.2 format) containing variant positions. It should contain at least the first 9 columns listing the SNP positions, sample-specific genotype calls across the sampleset are not required. Positional mandatory argument, should be the **third** argument after ``smap haplotype-sites``.
	  | ``-p``, ``--processes`` :white:`###########` *(int)* :white:`###` Number of parallel processes [1].
	  | ``--plot`` :white:`#########################` Select which plots are to be generated. Choosing "nothing" disables plot generation. Passing "summary" only generates graphs with information for all samples while "all" will also enable generate per-sample plots [default "summary"].
	  | ``-t``, ``--plot_type`` :white:`##################` Use this option to choose plot format, choices are png and pdf [png].  
	  | ``-o``, ``--out`` :white:`###############` *(str)* :white:`###` Basename of the output file without extension [SMAP_haplotype_sites].
	  | ``-u``, ``--undefined_representation`` :white:`#######` Value to use for non-existing or masked data [NaN].
	  | ``-h``, ``--help`` :white:`#####################` Show the full list of options. Disregards all other parameters.
	  | ``-v``, ``--version`` :white:`###################` Show the version. Disregards all other parameters.
	  | ``--debug`` :white:`########################` Enable verbose logging.
	  | 
	  | Options may be given in any order.	  
	  
   .. tab:: filtering options

	  | ``-q``, ``--min_mapping_quality`` :white:`####` *(int)* :white:`###` Minimum .bam mapping quality to retain reads for analysis [30].
	  | ``--no_indels`` :white:`#####################` Use this option if you want to **exclude** haplotypes that contain an InDel at the given SNP/SMAP positions. These reads are also ignored to evaluate the minimum read count [default off; indels are included in output].
	  | ``-j``, ``--min_distinct_haplotypes`` :white:`#` *(int)* :white:`###` Minimum number of distinct haplotypes per locus across all samples. Loci that do not fit this criterium are removed from the final output [0].
	  | ``-k``, ``--max_distinct_haplotypes`` :white:`#` *(int)* :white:`###` Maximum number of distinct haplotypes per locus across all samples. Loci that do not fit this criterium are removed from the final output [inf].
	  | ``-c``, ``--min_read_count`` :white:`#######` *(int)* :white:`###` Minimum total number of reads per locus per sample [0].
	  | ``-d``, ``--max_read_count`` :white:`#######` *(int)* :white:`###` Maximum number of reads per locus per sample, read count is calculated after filtering out the low frequency haplotypes (``-f``) [inf].
	  | ``-f``, ``--min_haplotype_frequency`` :white:`#` *(float)* :white:`##` Set minimum HF (in %) to retain the haplotype in the genotyping matrix. Haplotypes above this threshold in at least one of the samples are retained. Haplotypes that never reach this threshold in any of the samples are removed [0].
	  | ``-m``, ``--mask_frequency`` :white:`#######` *(float)* :white:`##` Mask haplotype frequency values below this threshold for individual samples to remove noise from the final output. Haplotype frequency values below this threshold are set to ``-u``. Haplotypes are not removed based on this value, use ``--min_haplotype_frequency`` for this purpose instead.
	  | 
	  | Options may be given in any order.	  

	  
   .. tab:: options for discrete calling in individual samples
	  
	   This option is primarily supported for diploids and tetraploids. Users can define their own custom frequency bounds for species with a higher ploidy, but this requires optimization based on the observed haplotype frequency distributions.
	  
	  ``-e``, ``–-discrete_calls`` :white:`###` *(str)* :white:`###` Set to "dominant" to transform haplotype frequency values into presence(1)/absence(0) calls per allele, or "dosage" to indicate the allele copy number.
	  
	  ``-i``, ``--frequency_interval_bounds`` :white:`##` Frequency interval bounds for classifying the read frequencies into discrete calls. Custom thresholds can be defined by passing one or more space-separated integers or floats which represent relative frequencies in percentage. For dominant calling, one value should be specified. For dosage calling, an even total number of four or more thresholds should be specified. Defaults are used by passing either "diploid" or "tetraploid". The default value for dominant calling (see discrete_calls argument) is 10, regardless whether or not "diploid" or "tetraploid" is used. For dosage calling, the default for diploids is "10 10 90 90" and for tetraploids "12.5 12.5 37.5 37.5 62.5 62.5 87.5 87.5"
	  
	  ``-z``, ``--dosage_filter`` :white:`###` *(int)* :white:`###` Mask dosage calls in the loci for which the total allele count for a given locus at a given sample differs from the defined value. For example, in diploid organisms the total allele copy number must be 2, and in tetraploids the total allele copy number must be 4. (default no filtering).
	 
	  ``--locus_correctness`` :white:`########` *(int)* :white:`###` Threshold value: % of samples with locus correctness. Create a new .bed file defining only the loci that were correctly dosage called (-z) in at least the defined percentage of samples (default no filtering).
	  
	  ``--frequency_interval_bounds`` **in practical examples and additional information on the dosage filter can be found** :ref:`here <SMAPhaplorec>`. 