from distutils.core import setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='SMAP',
    long_description=long_description,
    long_description_content_type="text/markdown",
    use_scm_version={'write_to': 'smap/version.py'},
    setup_requires=['setuptools_scm', 'setuptools_scm_git_archive'],
    description=('SMAP is a software package that analyzes "Stack-based"'
                 'Illumina short read data, such as Genotyping-By-Sequencing '
                 '(GBS) or multiplex amplicon sequencing, and extracts haplotype calls'),
    install_requires=['cython>=0.29.17', 'pybedtools>=0.8.1',
                      'pysam>=0.15.4', 'pandas>=1.2.2',
                      'matplotlib>=3.2.1', 'seaborn==0.10.1', 'colorlog>=4.6.2'],
    extras_require={'docs': ['sphinx','sphinx_rtd_theme','sphinx-tabs<2.0.0']},
    entry_points={
        'console_scripts': [
            'smap = smap.__main__:main',
        ],
        'modules': [
            'delineate = smap.delineate:main',
            'haplotype-sites = smap.haplotype:main',
            'compare = smap.compare:main'
        ]
    },
    data_files=[('bin', ['bin/bedtools'])],
    packages=['smap'],
    python_requires='>=3.7.1',
    url="https://gitlab.com/truttink/smap"
)
